module gitee.com/zxs-micro/zxs-micro-fuwufaxian

go 1.16

require (
	gitee.com/zxs-micro/zxs-micro-auth v0.0.1
	gitee.com/zxs-micro/zxs-micro-business v0.0.1
	gitee.com/zxs-micro/zxs-micro-common v0.0.1
	gitee.com/zxs-micro/zxs-micro-gateway v0.0.1
	github.com/pkg/errors v0.8.1
	github.com/spf13/viper v1.9.0
	google.golang.org/grpc v1.40.0
	google.golang.org/protobuf v1.27.1
)
